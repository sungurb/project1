# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](https://bitbucket.org/sungurb/project1/src/)
* [Demo video](https://youtu.be/yZ9syBY5zDQ)
* Description of projects and notes in README.md (this file). 


# Tempest Arcade Game

- This game inspired from 1980's arcade game, Tempest but of course it is not excatly the same. The game is to progress by killing enemies that come across a ship moving in a tunnel.
- There are levels in the game, now in the game consisting of **4 levels**, after reaching a certain score, it passes to the new level. At the end of the last level, the game is over or if you cannot kill the enemy and enemy leave the tunnel without being killed, the game is also over.
- For shotting the bullets, press space button. 
- The tunnel is divided into a certain number of segments. The tunnels are polygonal. The < and > keys are used to move through in tunnel. (I think I have some bug, sometimes it jumps couple segments after level changes)

-  I use *Sound library* when we shot bullets and hit the enemy. (2 different sound file).

- I actually have more features but I commended because I feel like it is too much feature. In game I use only bullets as a weapon but I actually made abstract class weapon and extends 2 different class, one of them is bullet the other one is bomb. 

- For the star class actually I take the code from Daniel Shiffmans starfield tutorial, I made couple changes but it is originally from there. I feel like it was okay to take because stars are not important thing in my game, I add them just because of visual reasons.


# Future Studies

- After submitting, I will apply new design patterns to the project beacuse I know It can be written better. I will continue  implementing the code.