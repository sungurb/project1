class Bullet
{
  private PVector pos, temp;
  private float r, x, y;
  private float bulSpeed, distance;

  Bullet(PVector position){

    pos = new PVector(position.x, position.y);
    temp = new PVector(0-pos.x, 0-pos.y);
    temp.normalize();
    bulSpeed = 6;
    r = 3;
  }

  void update(){

    pos.x += temp.x * bulSpeed;
    pos.y += temp.y * bulSpeed;
  }

  void display(){
    pushMatrix();
    fill(255,0,0);
    stroke(255,99,71);
    ellipseMode(CENTER);
    ellipse(pos.x, pos.y, r, r);
    popMatrix();
  }

  boolean isHitting(Enemy e){
    
    distance = dist(pos.x, pos.y, e.pos.x, e.pos.y);

    if(distance < 10 ){
      return true;
    }else return false;
  }

  boolean offEdges(BlackHole hole){
    
    float distance = dist(pos.x, pos.y, hole.pos.x, hole.pos.y);
    
    if(distance < hole.r + r) return true;
    else return false;

  }
}
// I will add this class later on p5.js for this project I decided not to use.
/*
abstract class Weapon  
{ 
  protected PVector pos, temp;
  protected float distance, bulSpeed, r;
  protected boolean canDraw;

  Weapon(PVector position){
    
    pos = new PVector(position.x, position.y);
    temp = new PVector(0-pos.x, 0-pos.y);
    temp.normalize();
    bulSpeed = 6;
    r = 4;
    canDraw = true;
  }

  void update(){

    pos.x += temp.x * bulSpeed;
    pos.y += temp.y * bulSpeed;
  }

  boolean isHitting(Enemy e){
    
    distance = dist(pos.x, pos.y, e.pos.x, e.pos.y);
    println(distance);

    if(distance < 10 ){
      return !canDraw;
    }else return canDraw;

  }

  boolean offEdges(BlackHole hole){
    
    float distance = dist(pos.x, pos.y, hole.pos.x, hole.pos.y);
    
    if(distance < hole.r + r) return true;
    else return false;
  }

  abstract void display();
}


class Bullet extends Weapon
{
  color col;

  Bullet(PVector position){
    super(position);
    col = color(255,69,0);
  }

  void display(){
    if(canDraw){
      pushMatrix();
      fill(col);
      noStroke();
      ellipseMode(CENTER);
      ellipse(pos.x, pos.y, r, r);
      popMatrix();
    }
  }
}

class Bomb extends Weapon
{
  float distance, r, bx, by;
  color col;

  Bomb(PVector position){

    super(position);
    r = 7;
    col = color(50,205,50);
  }

  void display(){
    pushMatrix();
    noFill();
    stroke(col);
    strokeWeight(2);
    beginShape();
    for (float a = 0 ; a < TWO_PI; a += angle) {
      bx = pos.x + cos(a) * r;
      by = pos.y + sin(a) * r;
      strokeWeight(2);
      vertex(bx, by);
      strokeWeight(1);
      line(pos.x, pos.y, bx, by);
    }
    endShape(CLOSE);
    popMatrix();
  }  
}*/
