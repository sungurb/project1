class RebelShip // Refer to Star Wars, ship that we are using
{
  PVector pos, vel;
  private float  bx, by, ax, ay, r, n ,x, y, pick;
  private float  tunAng;
  private int br, sr;
  private ArrayList<Float> angles;

  RebelShip( Tunnel tun){
    angles  = new ArrayList<Float>();
    r = tun.r;
    n = tun.n;
 
    tunAng = TWO_PI / tun.n;
    br = 40;
    sr = 25;

    bx =  cos(tunAng/2) * r;
    by =  sin(tunAng/2) * r;
    ax =  cos(tunAng/2 + tunAng) * r;
    ay =  sin(tunAng/2 + tunAng) * r;

    x = (bx - ax)/2 + ax;
    y = (by - ay)/2 + ay;
  
    pos = new PVector(x, y);
    
  }

  void display(){

    pushMatrix();
    fill(138,43,226,225);
    noStroke(); 
    quad(pos.x, pos.y+br/2,pos.x+br/2, pos.y, pos.x, pos.y-br/2, pos.x-br/2,pos.y);
    quad( pos.x-br/2,pos.y+ sr/2,  pos.x-br/2+sr/2, pos.y,  pos.x-br/2, pos.y-sr/2,  pos.x-br/2-sr/2, pos.y);
    quad( pos.x+br/2, pos.y+sr/2, pos.x+br/2+sr/2, pos.y,  pos.x+br/2, pos.y-sr/2,  pos.x+br/2-sr/2, pos.y);
    fill(153,50,204,200);
    quad(pos.x, pos.y+br/3, pos.x+br/3, pos.y, pos.x, pos.y-br/3, pos.x-br/3,pos.y);
    popMatrix();
  }

  void rot(){ //Calculating angles of tunnel and putting on array possible positions and iterating with arrow keys 
    for(float a = 0; a < TWO_PI; a+=tunAng) angles.add(a); 
    if(posIndex == r) posIndex = 0;
    if(posIndex == r - (r+1)) posIndex += (int)r;
    try {
       pick = angles.get(posIndex);
    } catch (IndexOutOfBoundsException e) {
      println("opps!");
    }
   
    pos.x = cos(pick) * (r-sr);
    pos.y = sin(pick) * (r-sr);
  }

  void resetPos(){
    pos.x = x;
    pos.y = y;
  }
}
