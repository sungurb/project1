/*
	Project 1
	Name of Project: Tempest Arcade Game
	Author: Beril Sungur
	Date: 31 May 2020
*/

import java.util.*;
import processing.sound.*;

SoundFile explode;
SoundFile shot;

final float speed = 2;
final int tunR = 400;
final color scoreTemplateCol = color(65,105,225);
final int INTRO = 0;
final int LEVELBASIC = 1;
final int LEVELMID = 2;
final int LEVELMIDHIGH = 3;
final int LEVELADVANCE = 4;
final int GAMEOVER = 5;

int posIndex;
Star[] stars = new Star[30];
int shots, score;
int mode = 0;

ArrayList<Bullet> bullets;
RebelShip ship, ship2, ship3, ship4;
Tunnel tunnel1, tunnel2, tunnel3, tunnel4;
GameEngine game;

void setup(){

  size(1000,1000);
  posIndex = 0;

  game = new GameEngine();
  tunnel1 = new Tunnel(6);
  tunnel2 = new Tunnel(8);
  tunnel3 = new Tunnel(10);
  tunnel4 = new Tunnel(12);
  ship = new RebelShip(tunnel1);
  ship2 = new RebelShip(tunnel2);
  ship3 = new RebelShip(tunnel3);
  ship4 = new RebelShip(tunnel4);  
  bullets = new ArrayList<Bullet>();


  for (int i = 0; i < stars.length; i++) {
    stars[i] = new Star();
  }

  shot = new SoundFile (this, "TIE fighter fire 1.wav");
  explode = new SoundFile (this, "XWing explode.wav");
}

void draw(){

  background(0);
  translate(width/2, height/2);
  gui();

  if (mode == INTRO)  game.intro();
  else if(mode == LEVELBASIC) game.level1(tunnel1, ship); 
  else if(mode == LEVELMID) game.level2(tunnel2, ship2);
  else if(mode == LEVELMIDHIGH) game.level3(tunnel3, ship3); 
  else if(mode == LEVELADVANCE) game.level4(tunnel4, ship4); 
  else if(mode == GAMEOVER) game.gameOver();

}

void keyPressed() 
{
  if (key == ' ') {
    if(mode == LEVELBASIC) bullets.add(new Bullet(ship.pos));
    else if (mode == LEVELMID) bullets.add(new Bullet(ship2.pos));
    else if (mode == LEVELMIDHIGH) bullets.add(new Bullet(ship3.pos));
    else if (mode == LEVELADVANCE) bullets.add(new Bullet(ship4.pos));
    shot.play();
    shots++;
  } 
  else if (keyCode == RIGHT) {
    posIndex --;
    if(mode == LEVELBASIC){
      if(posIndex == -1) posIndex += tunnel1.n;
      ship.rot();
    } 
    else if (mode == LEVELMID) {
      if(posIndex == -1) posIndex += tunnel2.n;
      ship2.rot();
    } 
    else if (mode == LEVELMIDHIGH){
      if(posIndex == -1) posIndex += tunnel3.n;
      ship3.rot();
    } 
    else if (mode == LEVELADVANCE){
      if(posIndex == -1) posIndex += tunnel4.n;
      ship4.rot();
    } 
  } 
  else if (keyCode == LEFT) {
    posIndex++;
    if(mode == LEVELBASIC) ship.rot();
    else if (mode == LEVELMID) ship2.rot();
    else if (mode == LEVELMIDHIGH) ship3.rot();
    else if (mode == LEVELADVANCE) ship4.rot();
  } 
}

void gui(){

  fill(scoreTemplateCol,200);
  noStroke();
  rect(-width/2+20, -height/2+30, 100,30); 
  rect(-width/2+20, -height/2+70, 100,30);
  fill(255);
  textSize(18);
  text("Score : " + score, -width/2+30, -height/2+50);
  text("Shots : " + shots, -width/2+30, -height/2+90);
}


class Level
{
  private color ebutton;
  private BlackHole hole;
  private ArrayList<Enemy> enemies;

  Level(){
    ebutton = color (148,0,211);
    hole = new BlackHole();
    enemies =  new ArrayList<Enemy>();
  }

  void welcoming(){
    score = 0;
    shots = 0;

    fill(255);
    textSize(46);
    text("Welcome to Tempest", -230, 0);
  }

  void addMeteor(Tunnel t, float rate){
    if(frameCount % rate == 0) enemies.add(new Meteor(t));
  }

  void addEmpireShip(Tunnel t, float rate){
    if(frameCount % rate == 0) enemies.add(new EmpireShips(t));
  }

  void addDeathStar(Tunnel t, float rate){
    if(frameCount % rate == 0) enemies.add(new DeathStar(t));
  }

  void play(Tunnel t, RebelShip s){

    for(int i = 0; i < enemies.size(); i++){
    Enemy enemy = enemies.get(i);
    enemy.display();
    enemy.directions();
    enemy.update();
      if(enemy.offTunnel()){
       mode = GAMEOVER;
      }
    }
    for (int i = bullets.size() - 1; i >= 0; i--) {
     Bullet bullet = (Bullet)bullets.get(i);
     bullet.display();
     bullet.update();
      for (int j = enemies.size() - 1; j >= 0; j--) {
       Enemy enemy = enemies.get(j);
        if(bullet.isHitting(enemy)){
         score++;
         enemies.remove(j);
         bullets.remove(i);
         explode.play();
        }
      } if(bullet.offEdges(hole)) bullets.remove(i); 
    }
    hole.display();
    t.display();
    s.display();
  } 

  void showStars(){

    for (int i = 0; i < stars.length; i++) {
      stars[i].update();
      stars[i].show();
    } 
  }
  void starting(int rate, int l){
    if(frameCount % rate == 0)  mode = l; 
  }

  void nextLevel(int points, int l){
    if(score == points){
      enemies.clear();
      mode = l;
    } 
  } 

  void reset(){
    enemies.clear();
    bullets.clear();
    ship.resetPos();

    if(key == ' ') mode = INTRO;
  }

  void overGui(){

    fill(255);
    textSize(60);
    text(" GAME OVER ", -225, 0);
    fill(ebutton, 200);
    rect(-205, 75, 350, 40);
    textSize(18);
    fill(255);
    text("Press The Space Button For Start Again", -200, 100);
  } 
}

class GameEngine
{
  Level l;

  GameEngine(){ 
    l = new Level();
  }

  void intro(){
    l.welcoming();
    l.showStars();
    l.starting(90, LEVELBASIC);
  }

  void level1(Tunnel t, RebelShip s){

    l.addMeteor(t, 180);
    l.play(t, s);
    l.showStars();
    l.nextLevel( 5, LEVELMID);
  }

  void level2(Tunnel t, RebelShip s){

    l.addMeteor(t, 150);
    l.addEmpireShip(t, 400);
    l.play(t,s);
    l.showStars();
    l.nextLevel( 15, LEVELMIDHIGH);
  }

  void level3(Tunnel t, RebelShip s){

    l.addMeteor(t, 180);
    l.addEmpireShip(t, 300);
    l.addDeathStar(t, 500);
    l.play(t,s);
    l.showStars();
    l.nextLevel( 30, LEVELADVANCE);
  }

  void level4(Tunnel t, RebelShip s){

    l.addMeteor(t, 120);
    l.addEmpireShip(t, 300);
    l.addDeathStar(t, 400);
    l.play(t,s);
    l.showStars();
    l.nextLevel( 50, GAMEOVER);
  }

  void gameOver(){
    l.reset();
    l.overGui();
  }
}


class BlackHole // Enemies comes inside blackhole and swallow bullets
{
  private PVector pos;
  private float r;

  BlackHole(){
    pos = new PVector(0,0);
    r = 15;
  }

  void display(){
    noStroke();
    noFill();
    ellipseMode(CENTER);
    ellipse(pos.x, pos.y, r, r);
  }
}


class Star { // just for visualization, code from starfield Daniel Shiffman
  private float x;
  private float y;
  private float z;
  private float pz;

  Star() {

    x = random(-width/2, width/2);
    y = random(-height/2, height/2);
    z = random(width/2);
    pz = z;
  }

  void update() {
    z = z - speed;
    if (z < 1) {
      z = width/2;
      x = random(-width/2, width/2);
      y = random(-height/2, height/2);
      pz = z;
    }
  }

  void show() {
    fill(255);
    noStroke();

    float sx = map(x / z, 0, 1, 0, width/2);
    float sy = map(y / z, 0, 1, 0, height/2);;
    float r = map(z, 0, width/2, 6, 0);
    ellipse(sx, sy, r, r);

    float px = map(x / pz, 0, 1, 0, width/2);
    float py = map(y / pz, 0, 1, 0, height/2);
    pz = z;

    stroke(255);
    line(px, py, sx, sy);
  }
}
