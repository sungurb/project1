abstract class Enemy
{
  protected PVector pos, direction;
  protected float pick, angle, a, r;
  protected int index;
  protected ArrayList<Float> angles;

  Enemy(Tunnel tun){
   pos = new PVector(0,0);
   angles  = new ArrayList<Float>();   
   angle = TWO_PI / tun.n;
   index = (int)random(tun.n);
  }

  void update(){
    pos.add(direction);
  }

  boolean offTunnel(){

    float distance = dist(pos.x, pos.y, 0, 0);
    if(distance > tunR*cos(angle/2)) return true;
    else return false;
  }

  abstract void display();
  abstract void points();
  abstract void directions();
}


class Meteor extends Enemy{

  private float r, br, s;
  private color fc, sc;

  Meteor(Tunnel tun){
    
    super(tun);
    s = 1.5; //speed
    fc = color(255,215,0);
    sc = color(255,255,0);
  }
  
  void display(){
     r = map(pos.x, 5, width/3, 0, 25);
     br = map(pos.y, 5, height/3, 0, 25);
     pushMatrix();
     fill(fc,40); 
     stroke(sc);
     strokeWeight(2);
     ellipseMode(CENTER);
     ellipse(pos.x, pos.y, br, br);
     ellipse(pos.x, pos.y, r, r);
     popMatrix();
  }

  void directions(){
    for(float a = 0; a < TWO_PI; a+=angle) angles.add(a);
    pick = angles.get(index);
    direction = new PVector(cos(pick)*s, sin(pick)*s);
  }

  void points(){
    shots++;
  }
}

class EmpireShips extends Enemy
{
  private float hr, wr, s, t;
    
  EmpireShips(Tunnel tun){
    
    super(tun);
    s = 2;
    wr = 20;
    hr = 10;
  }

  void display(){

    wr = map(abs(pos.x), 1, width/4, 0, 20);
    hr = map(abs(pos.y), 1, height/4, 0, 10); 
    
    pushMatrix();
    noFill();
    stroke(255,0,0);
    strokeWeight(2);
    quad(pos.x, pos.y+wr/2,pos.x+wr/2, pos.y, pos.x, pos.y-wr/2, pos.x-wr/2,pos.y);
    quad(pos.x, pos.y+hr/2,pos.x+hr/2, pos.y, pos.x, pos.y-hr/2, pos.x-hr/2,pos.y);
    popMatrix();
  }

  void directions(){

    for(float a = 0; a < TWO_PI; a+=angle) angles.add(a);
    pick = angles.get(index);
    direction = new PVector(cos(pick)*s, sin(pick)*s);
  }

  int points(){
    shots += 5;
  }
}

class DeathStar extends Enemy
{
  float s, br, sr;

  DeathStar(Tunnel tun){
    super(tun);
    s = 2.5;
        
  }

  void display(){

    br = map(abs(pos.y), 1, height/3, 5, 25);
    sr = map(abs(pos.y), 1, height/4, 3, 15); 

    pushMatrix();
    fill(255,99,71,30);
    stroke(50,205,50);
    strokeWeight(2);
    ellipseMode(CENTER);
    ellipse(pos.x, pos.y, br, br);
    ellipse(pos.x, pos.y, sr, sr);
    line(pos.x+br, pos.y, pos.x-br, pos.y);
    popMatrix();
  }

  void directions(){
    for(float a = 0; a < TWO_PI; a+=angle) angles.add(a);
    pick = angles.get(index);
    direction = new PVector(cos(pick)*s, sin(pick)*s);
  }
 
  void points(){
    shots += 10;
  }
}
