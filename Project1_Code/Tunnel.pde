class Tunnel
{
  private float x, y, r;
  private float angle;
  private int n;
  private float sx, sy, bx, by;

  Tunnel(int n){
    x = 0;
    y = 0;
    r = tunR;
    this.n = n;
    angle = TWO_PI / n;
  }

  void display(){ 

    pushMatrix();
    rotate(angle/2);
    noFill();
    stroke(0,0,205); 
    
    beginShape();
    for (float a =0 ; a < TWO_PI; a += angle) {
      bx = x + cos(a) * r;
      by = y + sin(a) * r;
      strokeWeight(5);
      vertex(bx, by);
    }
    endShape(CLOSE);

    beginShape();
    for (float a = 0; a < TWO_PI; a += angle) {
      sx = x + cos(a) * r / 6;
      sy = y + sin(a) * r / 6;
      strokeWeight(2);
      vertex(sx, sy);
    }
    endShape(CLOSE);

    for(float a = 0; a < TWO_PI; a+=angle){
     bx = x + cos(a) * r;
     by = y + sin(a) * r;
     sx = x + cos(a) * r / 6;
     sy = y + sin(a) * r / 6;
     strokeWeight(3);
     line(bx,by,sx,sy);
    }
    popMatrix();
  }
}
